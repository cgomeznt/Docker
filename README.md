## Docker


* [Step-step con Dockerfile](guia/Step_step_con_Dockerfile.rst)
* [Step-step con Dockerfile para Weblogic](guia/Step_step_con_Dockerfile_para_Weblogic.rst)
* [Export and Import a Docker Image Between Nodes](guia/Export_and_Import_a_Docker_Image_Between_Nodes.rst)
* [Pasos Actualizar Ambiente](guia/Pasos_Actualizar_Ambiente.rst)
* [Docker paso a paso para CONSIS con WebLogic](guia/Docker_paso_paso_CONSIS_WebLogic.rst)
* [Docker paso a paso para CONSIS con WAS](guia/Docker_paso_paso_CONSIS_WAS.rst)
* [Un buen build run exec para CONSIS](guia/build-run-exec.rst)






